program mvpl;

{$mode objfpc}
{$H+}// 'string' means 'AnsiString'

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}{$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  MainWindowForm,
  filenameform,
  CommandManager,
  Commands,
  historyManager,
  ScanProgressForm,
  ApplicationExceptions;

  {$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TMainWindow, MainWindow);
  Application.Run;
end.
