unit ScanProgressForm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  LinkManager;

type

  { TScanProgressDialog }

  TScanProgressDialog = class(TForm)
    CancelButton: TButton;
    Message: TLabel;
    ProgressBar: TProgressBar;
    procedure CancelButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
  private
    fLinkManager: TLinkManager;
    fOnScanFinished: TScanFinishedHandler;
  public
    constructor Create(TheOwner: TComponent; const aLinkManager: TLinkManager;
      const aOnScanFinished: TScanFinishedHandler); reintroduce;
    procedure OnScanFinished(const aSuccess: boolean; const aException: Exception);
  end;

var
  ScanProgressDialog: TScanProgressDialog;

implementation

{$R *.lfm}

{ TScanProgressDialog }

constructor TScanProgressDialog.Create(TheOwner: TComponent;
  const aLinkManager: TLinkManager; const aOnScanFinished: TScanFinishedHandler);
begin
  inherited Create(TheOwner);
  fLinkManager := aLinkManager;
  fOnScanFinished := aOnScanFinished;
end;

procedure TScanProgressDialog.FormCreate(Sender: TObject);
begin
  fLinkManager.Scan(@OnScanFinished);
end;

procedure TScanProgressDialog.OnScanFinished(const aSuccess: boolean;
  const aException: Exception);
begin
  fOnScanFinished(aSuccess, aException);
  if aSuccess then
    ModalResult := mrOk
  else
    ModalResult := mrCancel;
end;

procedure TScanProgressDialog.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  CanClose := ModalResult <> mrNone;
end;

procedure TScanProgressDialog.CancelButtonClick(Sender: TObject);
begin
  fLinkManager.CancelScan();
end;

end.
