unit FilenameForm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TFilenameDialog }

  TFilenameDialog = class(TForm)
    CancelButton: TButton;
    OKButton: TButton;
    NewName: TEdit;
    Message: TLabel;
    constructor Create(TheOwner: TComponent; const aMessage: string); reintroduce;
    procedure CancelButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure OKButtonClick(Sender: TObject);
  private

  public

  end;

var
  FilenameDialog: TFilenameDialog;

implementation

{$R *.lfm}

{ TFilenameDialog }

constructor TFilenameDialog.Create(TheOwner: TComponent; const aMessage: string);
begin
  inherited Create(TheOwner);
  Message.Caption := aMessage;
end;

procedure TFilenameDialog.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  CanClose := ModalResult <> mrNone;
end;

procedure TFilenameDialog.CancelButtonClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFilenameDialog.OKButtonClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
