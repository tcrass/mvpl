unit MainWindowForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ComCtrls,
  EditBtn, StdCtrls, ExtCtrls, ShellCtrls, ActnList, Generics.Defaults, LCLType,
  LinkManager, CommandManager, HistoryManager;

const
  itDirectory = 'directory';
  itSymlink = 'symlink';

  sbMessage = 0;
  sbSelectedPath = 1;

type
  TProc = procedure of object;
  PProc = ^TProc;

  TActionType = (atMove, atCopy, atLink);

  TActionInfo = record
    ActionType: TActionType;
    Path: string;
    DestinationPath: string;
    PathType: TPathType;
  end;

  { https://forum.lazarus.freepascal.org/index.php?topic=45766.0 }

  TShellTreeViewOpener = class(TShellTreeView);

  THistory = specialize THistoryManager<string>;

  { TMainWindow }

  TMainWindow = class(TForm)
    CopyHereAction: TAction;
    CreateDirectoryAction: TAction;
    GoOpenTargetItem: TMenuItem;
    CreateDirectoryItem: TMenuItem;
    EditCreateDirectoryItem: TMenuItem;
    CopyHereItem: TMenuItem;
    OpenTargetItem: TMenuItem;
    OpenTargetAction: TAction;
    Separator2: TMenuItem;
    Separator3: TMenuItem;
    Separator4: TMenuItem;
    Separator5: TMenuItem;
    Separator6: TMenuItem;
    SetAsRootAction: TAction;
    LinkHereAbsoluteAction: TAction;
    LinkHereRelativeAction: TAction;
    MoveHereAction: TAction;
    RefreshAction: TAction;
    ForwardAction: TAction;
    BackAction: TAction;
    UpAction: TAction;
    DeletePermanentlyAction: TAction;
    MoveToTrashAction: TAction;
    RenameAction: TAction;
    RedoAction: TAction;
    UndoAction: TAction;
    QuitAction: TAction;
    ActionList: TActionList;
    SetRootButton: TButton;
    CanelItem: TMenuItem;
    DropPopupMenu: TPopupMenu;
    FileList: TShellListView;
    FileTree: TShellTreeView;
    FileIcons: TImageList;
    ActionIcons: TImageList;
    LinkHereRelativeItem: TMenuItem;
    MainMenu: TMainMenu;
    LinkHereAbsoluteItem: TMenuItem;
    DeletePermanentlyItem: TMenuItem;
    FileItem: TMenuItem;
    EditItem: TMenuItem;
    FileQuitItem: TMenuItem;
    EditRenameItem: TMenuItem;
    EditMoveToTrashItem: TMenuItem;
    EditDeletePermanentlyItem: TMenuItem;
    EditUndoItem: TMenuItem;
    EditRedoItem: TMenuItem;
    GoItem: TMenuItem;
    GoUpItem: TMenuItem;
    GoBackItem: TMenuItem;
    GoForwardItem: TMenuItem;
    Separator1: TMenuItem;
    StatusBar: TStatusBar;
    BackButton: TToolButton;
    ForwardButton: TToolButton;
    UpButton: TToolButton;
    ViewRefreshItem: TMenuItem;
    ViewItem: TMenuItem;
    MoveToTrashItem: TMenuItem;
    RenameItem: TMenuItem;
    MoveHereItem: TMenuItem;
    FileContextMenu: TPopupMenu;
    RootDirectoryEdit: TDirectoryEdit;
    RootLabel: TLabel;
    Splitter: TSplitter;
    ToolBar: TToolBar;
    RefreshButton: TToolButton;
    RedoButton: TToolButton;
    UndoButton: TToolButton;
    { Main window}
    procedure CopyHereActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure GlobalExceptionHandler(Sender: TObject; E: Exception);
    procedure RootDirectoryEditChange(Sender: TObject);
    procedure OnScanFinished(const aSuccess: boolean; const aException: Exception);
    { File tree }
    function FileTreeSortCompare(Item1, Item2: TFileItem): integer;
    procedure FileTreeGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure FileTreeGetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure FileTreeSelectionChanged(Sender: TObject);
    procedure FileTreeDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure FileTreeDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure FileTreeEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure FileTreeEditing(Sender: TObject; Node: TTreeNode; var AllowEdit: boolean);
    function FileTreeGetSelectedPath: string;
    function FileTreeGetPath(const aNode: TTreeNode): string; overload;
    procedure FileTreeUpdateView();
    { File list }
    procedure FileListSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
    procedure FileListDblClick(Sender: TObject);
    procedure FileListFileAdded(Sender: TObject; Item: TListItem);
    procedure FileListCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: integer; var Compare: integer);
    procedure FileListMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure FileListDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure FileListDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure FileListEdited(Sender: TObject; Item: TListItem; var AValue: string);
    procedure FileListEditing(Sender: TObject; Item: TListItem; var AllowEdit: boolean);
    function FileListGetSelectedPath: string;
    function FileListGetPath(aItem: TListItem): string; overload;
    procedure FileListSelectFilename(const aFilename: string);
    procedure FileListSelectSelectedLinkTarget();
    function FileListIsDirectory(aItem: TListItem): boolean;
    { Actions }
    procedure QuitActionExecute(Sender: TObject);
    procedure SetAsRootActionExecute(Sender: TObject);
    procedure UndoActionExecute(Sender: TObject);
    procedure RedoActionExecute(Sender: TObject);
    procedure RenameActionExecute(Sender: TObject);
    procedure MoveToTrashActionExecute(Sender: TObject);
    procedure DeletePermanentlyActionExecute(Sender: TObject);
    procedure CreateDirectoryActionExecute(Sender: TObject);
    procedure BackActionExecute(Sender: TObject);
    procedure ForwardActionExecute(Sender: TObject);
    procedure UpActionExecute(Sender: TObject);
    procedure OpenTargetActionExecute(Sender: TObject);
    procedure RefreshActionExecute(Sender: TObject);
    procedure MoveHereActionExecute(Sender: TObject);
    procedure LinkHereRelativeActionExecute(Sender: TObject);
    procedure LinkHereAbsoluteActionExecute(Sender: TObject);
    { File context menu }
    procedure FileContextMenuPopup(Sender: TObject);
    { Drop context menu }
    procedure CanelDropItemClick(Sender: TObject);
  private
    fSelectedPath: string;
    fSelectedFileInfo: TFileInfo;
    fSelectedLinkInfo: TLinkInfo;
    fActionInfo: TActionInfo;
    fDraggedItem: TListItem;
    fLinkManager: TLinkManager;
    fCommandManager: TCommandManager;
    fRoot: string;
    fScanning: boolean;
    fScanSuccess: boolean;
    fHistory: THistory;
    { Property accessors }
    procedure SetRoot(const aValue: string);
    procedure SetScanning(const aValue: boolean);
    procedure SetScanSuccess(const aValue: boolean);
    procedure SetSelectedPath(const aValue: string);
    procedure SetStatusBarMessage(const aValue: string);
    procedure SetStatusBarPath(const aValue: string);
    { Properties }
    property Root: string read fRoot write SetRoot;
    property Scanning: boolean read fScanning write SetScanning;
    property ScanSuccess: boolean read fScanSuccess write SetScanSuccess;
    property SelectedPath: string read fSelectedPath write SetSelectedPath;
    property SelectedFileInfo: TFileInfo read fSelectedFileInfo;
    property SelectedLinkInfo: TLinkInfo read fSelectedLinkInfo;
    property StatusBarMessage: string write SetStatusBarMessage;
    property StatusBarPath: string write SetStatusBarPath;
    { Other stuff }
    function GetClosestExistingDirectory(const aPath: string;
      const aBaseDir: string = '/'): string;
    procedure ShowException(const aException: Exception);
    procedure ExecuteLater(aProcedure: TProc);
    procedure DoExecute(Data: PtrInt);
    procedure StartScan();
    procedure UpdateUI();
    procedure UpdateView();
    procedure UpdateActionExecutabilities();
    procedure MoveOrLinkIntoDirectory(const aSourcePath: string;
      const aDestinationDirectory: string);
    procedure MoveOrLinkTo(const aSourcePath: string; const aDestinationPath: string);
    function ExecuteAction(var aActionInfo: TActionInfo): boolean;
    function RequestNewName(var aName: string): boolean;
    procedure MoveTo(const aSourcePath: string; const aDestinationPath: string);
    procedure CopyTo(const aSourcePath: string; const aDestinationPath: string);
    procedure LinkAs(const aLinkTargetPath: string; const aLinkedAsPath: string;
      const aPathType: TPathType);
    function GetImageIndex(const aPath: string): integer;
    function GetImageIndex(aFileType: integer): integer;
  end;

var
  MainWindow: TMainWindow;

implementation

{$R *.lfm}

uses LazUtils, LazFileUtils,
  Commands, ApplicationExceptions, ScanProgressForm, FilenameForm;

  (* --- Main window ------------------------------------------------- *)

  (* --- Property accessors --- *)

procedure TMainWindow.SetRoot(const aValue: string);
var
  baseDir: string;
begin
  baseDir := ExcludeTrailingPathDelimiter(aValue);
  fRoot := baseDir;
  fLinkManager.Root := baseDir;
  RootDirectoryEdit.Directory := baseDir;
  FileTree.Root := baseDir;
  ScanSuccess := False;
  fHistory.Clear();
end;

procedure TMainWindow.SetScanning(const aValue: boolean);
begin
  fScanning := aValue;
  UpdateUI();
end;

procedure TMainWindow.SetScanSuccess(const aValue: boolean);
begin
  fScanSuccess := aValue;
  FileTreeUpdateView();
  SelectedPath := '';
end;

procedure TMainWindow.SetSelectedPath(const aValue: string);
var
  dir: string;
begin
  if (aValue <> '') then
  begin
    dir := GetClosestExistingDirectory(aValue, Root);
    if (dir <> '') and (fHistory.Current <> dir) then
      fHistory.Add(aValue);
  end;
  fSelectedPath := aValue;
  fSelectedFileInfo := fLinkManager.GetFileInfo(fSelectedPath);
  fSelectedLinkInfo := fLinkManager.GetLinkInfo(fSelectedPath);
  UpdateUI();
end;

procedure TMainWindow.SetStatusBarMessage(const aValue: string);
begin
  StatusBar.Panels[sbMessage].Text := aValue;
end;

procedure TMainWindow.SetStatusBarPath(const aValue: string);
begin
  StatusBar.Panels[sbSelectedPath].Text := aValue;
end;

(* --- Construction / destruction --- *)

procedure TMainWindow.FormCreate(Sender: TObject);
var
  myRoot: string;
begin
  Application.OnException := @GlobalExceptionHandler;

  (* First cast to Pointer?! Ugly... *)
  TShellTreeViewOpener(Pointer(FileTree)).OnDragOver := @FileTreeDragOver;
  TShellTreeViewOpener(Pointer(FileTree)).OnDragDrop := @FileTreeDragDrop;

  fCommandManager := TCommandManager.Create();
  fLinkManager := TLinkManager.Create();
  fHistory := THistory.Create(False);

  Scanning := False;
  ScanSuccess := False;

  myRoot := GetUserDir();

  {$ifopt D+}
  myRoot := CleanAndExpandDirectory(IncludeTrailingPathDelimiter(GetCurrentDirUTF8()) +
    '../testRoot');
  {$endif}

  Root := myRoot;
end;

procedure TMainWindow.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  fHistory.Free();
  fLinkManager.Free();
  fCommandManager.Free();
end;

(* --- Other methods --- *)

procedure TMainWindow.GlobalExceptionHandler(Sender: TObject; E: Exception);
begin
  ShowException(E);
end;

function TMainWindow.GetClosestExistingDirectory(const aPath: string;
  const aBaseDir: string = '/'): string;
var
  path: string;
begin
  path := aPath;
  while not (DirectoryExists(path)) and (path <> aBaseDir) and (path <> '') and
    (path <> '/') do
    path := ExtractFileDir(path);
  GetClosestExistingDirectory := path;
end;

procedure TMainWindow.ShowException(const aException: Exception);
begin
  writeln('*** Exception: ' + aException.Message);
  QuestionDlg('Error', aException.Message, mtError, [mrOk, 'IsDefault'], 0);
  if (aException is ERecoverable) then
  begin
    FileTreeUpdateView();
    UpdateUI();
    StatusBarMessage := aException.Message;
  end
  else
  begin
    writeln('Exception is fatal, will halt.');
    halt;
  end;
end;

procedure TMainWindow.StartScan();
var
  scanProgressDialog: TScanProgressDialog;
begin
  ScanSuccess := False;
  Scanning := True;
  scanProgressDialog := TScanProgressDialog.Create(self, fLinkManager, @OnScanFinished);
  scanProgressDialog.ShowModal();
  scanProgressDialog.Free();
end;

procedure TMainWindow.RootDirectoryEditChange(Sender: TObject);
begin
  SetRootButton.Enabled := DirPathExists(RootDirectoryEdit.Directory);
end;

procedure TMainWindow.OnScanFinished(const aSuccess: boolean; const aException: Exception);
begin
  Scanning := False;
  ScanSuccess := aSuccess;
  if Assigned(aException) then
    ShowException(aException);
end;

(* --- File tree --- *)

function TMainWindow.FileTreeSortCompare(Item1, Item2: TFileItem): integer;
begin
  FileTreeSortCompare := CompareFilenamesIgnoreCase(Item1.FileInfo.Name, Item2.FileInfo.Name);
end;

function TMainWindow.FileTreeGetSelectedPath(): string;
begin
  if Assigned(FileTree.Selected) then
    FileTreeGetSelectedPath := FileTreeGetPath(FileTree.Selected)
  else
    FileTreeGetSelectedPath := '';
end;

function TMainWindow.FileTreeGetPath(const aNode: TTreeNode): string;
begin
  FileTreeGetPath := TShellTreeNode(aNode).FullFilename;
end;

procedure TMainWindow.FileTreeGetImageIndex(Sender: TObject; Node: TTreeNode);
begin
  Node.ImageIndex := GetImageIndex(TShellTreeNode(Node).FullFilename);
end;

procedure TMainWindow.FileTreeGetSelectedIndex(Sender: TObject; Node: TTreeNode);
begin
  Node.SelectedIndex := GetImageIndex(TShellTreeNode(Node).FullFilename);
end;

procedure TMainWindow.FileTreeDragOver(Sender, Source: TObject; X, Y: integer;
  State: TDragState; var Accept: boolean);
var
  dropTarget: TTreeNode;
begin
  dropTarget := FileTree.GetNodeAt(X, Y);
  Accept := Assigned(dropTarget) and (FileTreeGetPath(dropTarget) <>
    FileListGetPath(fDraggedItem));
  (* Why does this not work?! *)
  if Accept then
  begin
    TShellTreeViewOpener(Pointer(FileTree)).DragCursor := crDrag;
    //FileList.DragCursor := crDrag;
    //DragCursor := crDrag;
  end
  else
  begin
    TShellTreeViewOpener(Pointer(FileTree)).DragCursor := crNoDrop;
    //FileList.DragCursor := crNoDrop;
    //DragCursor := crNoDrop;
  end;
end;

procedure TMainWindow.FileTreeDragDrop(Sender, Source: TObject; X, Y: integer);
var
  dropTarget: TTreeNode;
begin
  dropTarget := FileTree.GetNodeAt(X, Y);
  MoveOrLinkIntoDirectory(FileListGetPath(fDraggedItem), FileTreeGetPath(dropTarget));
end;

procedure TMainWindow.FileTreeEditing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: boolean);
begin
  AllowEdit := Node <> FileTree.Items.GetFirstNode;
  if AllowEdit then
  begin
    fActionInfo.ActionType := atMove;
    fActionInfo.Path := FileTreeGetPath(Node);
  end;
end;

procedure TMainWindow.FileTreeEdited(Sender: TObject; Node: TTreeNode; var S: string);
begin
  fActionInfo.DestinationPath := ExtractFilePath(FileTreeGetPath(Node)) + S;
  if not ExecuteAction(fActionInfo) then
    S := Node.Text;
  UpdateUI();
  ExecuteLater(@FileTreeUpdateView);
end;

procedure TMainWindow.FileTreeSelectionChanged(Sender: TObject);
begin
  SelectedPath := FileTreeGetSelectedPath;
end;

procedure TMainWindow.FileTreeUpdateView();

  procedure RecordNodeState(const node: TTreeNode; const expandedPaths: TStringList);
  var
    currentNode: TTreeNode;
    firstChild: TTreeNode;
  begin
    currentNode := node;
    while Assigned(currentNode) do
    begin
      if currentNode.Expanded then
      begin
        expandedPaths.Add(FileTree.GetPathFromNode(currentNode));
        firstChild := currentNode.GetFirstChild();
        if Assigned(firstChild) then
          RecordNodeState(firstChild, expandedPaths);
      end;
      currentNode := currentNode.GetNextSibling();
    end;
  end;

  procedure RestoreNodeState(const node: TTreeNode; const refresh: boolean;
  const expandedPaths: TStringList);
  var
    currentNode: TTreeNode;
    firstChild: TTreeNode;
  begin
    currentNode := node;
    while Assigned(currentNode) do
    begin
      if expandedPaths.IndexOf(FileTree.GetPathFromNode(currentNode)) >= 0 then
      begin
        currentNode.Expanded := True;
        if refresh then
          FileTree.Refresh(currentNode);
        firstChild := currentNode.GetFirstChild();
        if Assigned(firstChild) then
          RestoreNodeState(firstChild, refresh, expandedPaths);
      end
      else
        currentNode.Expanded := False;
      currentNode := currentNode.GetNextSibling();
    end;
  end;

var
  firstNode: TTreeNode;
  selectedFilePath: string;
  expandedPaths: TStringList;
  selectedFile: string;
begin
  expandedPaths := TStringList.Create();

  selectedFilePath := FileTree.GetPathFromNode(FileTree.Selected);
  if Assigned(FileList.Selected) then
    selectedFile := FileList.Selected.Caption
  else
    selectedFile := '';

  firstNode := FileTree.Items.GetFirstNode();
  RecordNodeState(firstNode, expandedPaths);
  RestoreNodeState(firstNode, True, expandedPaths);

  try
    FileTree.Path := GetClosestExistingDirectory(selectedFilePath, Root);
  except
    on EInvalidPath do
      FileTree.Path := '';
  end;
  { Setting the path apparently also expands the selecte node, so
    re-apply the recorded node state, but don't refresh nodes }
  RestoreNodeState(firstNode, False, expandedPaths);

  { Force synchronization of associated TShellListView }
  FileTree.ShellListView := nil;
  FileTree.ShellListView := FileList;
  FileListSelectFilename(selectedFile);

  expandedPaths.Free();
end;

(* --- File list --- *)

function TMainWindow.FileListGetSelectedPath: string;
begin
  if Assigned(FileList.Selected) then
    FileListGetSelectedPath := FileListGetPath(FileList.Selected);
end;

function TMainWindow.FileListGetPath(aItem: TListItem): string;
begin
  FileListGetPath := FileList.GetPathFromItem(aItem);
end;

procedure TMainWindow.FileListSelectFilename(const aFilename: string);
begin
  FileList.Selected := FileList.FindCaption(0, aFilename, False, True, False);
end;

procedure TMainWindow.FileListSelectSelectedLinkTarget();
var
  filename: string;
begin
  filename := ExtractFileName(SelectedLinkInfo.Target);
  FileListSelectFilename(filename);
end;

function TMainWindow.FileListIsDirectory(aItem: TListItem): boolean;
begin
  FileListIsDirectory := aItem.SubItems.IndexOf(itDirectory) >= 0;
end;

procedure TMainWindow.FileListFileAdded(Sender: TObject; Item: TListItem);
var
  path: string;
  fileType: integer;
begin
  path := FileListGetPath(Item);
  Item.SubItems.Add(path);
  fileType := TLinkManager.GetFileTypeOf(path);
  if (fileType and ftDirectory) = ftDirectory then
    Item.SubItems.Add(itDirectory);
  if (fileType and ftSymlink) = ftSymlink then
    Item.SubItems.Add(itSymlink);
  Item.ImageIndex := GetImageIndex(fileType);
  FileList.AlphaSort;
end;

procedure TMainWindow.FileListCompare(Sender: TObject; Item1, Item2: TListItem;
  Data: integer; var Compare: integer);
var
  item1IsDirectory: boolean;
  item2IsDirectory: boolean;
begin
  if FileList.SortColumn < 1 then
  begin
    item1IsDirectory := FileListIsDirectory(Item1);
    item2IsDirectory := FileListIsDirectory(Item2);
    if item1IsDirectory and not item2IsDirectory then
      Compare := -1
    else if not item1IsDirectory and item2IsDirectory then
      Compare := 1
    else
      Compare := CompareFilenamesP(PChar(Item1.Caption), PChar(Item2.Caption), True);
  end
  else
    Compare := CompareFilenamesP(PChar(Item1.SubItems[FileList.SortColumn - 1]),
      PChar(Item2.SubItems[FileList.SortColumn - 1]), True);
end;


procedure TMainWindow.FileListSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
  if Selected then
  begin
    SelectedPath := FileListGetSelectedPath;
    UpdateUI();
  end;
end;


procedure TMainWindow.FileListDblClick(Sender: TObject);
var
  clickedAt: TPoint;
  item: TListItem;
begin
  clickedAt := FileList.ScreenToClient(Mouse.CursorPos);
  item := FileList.GetItemAt(clickedAt.X, clickedAt.Y);
  if Assigned(item) and FileListIsDirectory(item) then
    FileTree.Path := CreateRelativePath(FileListGetPath(item), FileTree.Root);
end;

procedure TMainWindow.FileListMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  if Button = mbLeft then
  begin
    fDraggedItem := FileList.GetItemAt(X, Y);
    if Assigned(fDraggedItem) then
      FileList.BeginDrag(False, 4);
  end;
end;

procedure TMainWindow.FileListDragOver(Sender, Source: TObject; X, Y: integer;
  State: TDragState; var Accept: boolean);
var
  dropTarget: TListItem;
begin
  dropTarget := FileList.GetItemAt(X, Y);
  Accept := not Assigned(dropTarget) or (FileListIsDirectory(dropTarget) and
    (FileListGetPath(dropTarget) <> FileListGetPath(fDraggedItem)));
end;

procedure TMainWindow.FileListDragDrop(Sender, Source: TObject; X, Y: integer);
var
  dropTarget: TListItem;
begin
  dropTarget := FileList.GetItemAt(X, Y);
  if Assigned(dropTarget) then
    MoveOrLinkIntoDirectory(FileListGetPath(fDraggedItem), FileListGetPath(dropTarget))
  else
    MoveOrLinkTo(FileListGetPath(fDraggedItem), FileListGetPath(fDraggedItem));
end;


procedure TMainWindow.FileListEditing(Sender: TObject; Item: TListItem;
  var AllowEdit: boolean);
begin
  fActionInfo.ActionType := atMove;
  fActionInfo.Path := FileListGetPath(Item);
end;

procedure TMainWindow.FileListEdited(Sender: TObject; Item: TListItem; var AValue: string);
begin
  fActionInfo.DestinationPath := ExtractFilePath(FileList.GetPathFromItem(Item)) + AValue;
  if not ExecuteAction(fActionInfo) then
    AValue := Item.Caption;
  UpdateUI();
  ExecuteLater(@FileTreeUpdateView);
end;

(* --- Actions --- *)

procedure TMainWindow.QuitActionExecute(Sender: TObject);
begin
  Close();
end;

procedure TMainWindow.SetAsRootActionExecute(Sender: TObject);
begin
  Root := RootDirectoryEdit.Directory;
  StartScan();
end;


procedure TMainWindow.UndoActionExecute(Sender: TObject);
begin
  writeln('*** Undo');
  fCommandManager.Undo();
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.RedoActionExecute(Sender: TObject);
begin
  writeln('*** Redo');
  fCommandManager.Redo();
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.RenameActionExecute(Sender: TObject);
var
  filenameDialog: TFilenameDialog;
  filename: string;
begin
  writeln('*** Rename');
  filename := ExtractFileNameOnly(SelectedPath);
  filenameDialog := TFilenameDialog.Create(self, 'Please enter what "' +
    filename + '" should be renamed to.');
  filenameDialog.NewName.Text := filename;
  if filenameDialog.ShowModal() = mrOk then
  begin
    filename := filenameDialog.NewName.Text;
    fActionInfo.ActionType := atMove;
    fActionInfo.Path := SelectedPath;
    fActionInfo.DestinationPath := ExtractFilePath(SelectedPath) + filename;
    ExecuteAction(fActionInfo);
    FileTreeUpdateView();
    UpdateUI();
  end;
end;

procedure TMainWindow.MoveToTrashActionExecute(Sender: TObject);
begin
  writeln('*** Move To Trash');
  fCommandManager.Execute(TTrashCommand.Create(fLinkManager, SelectedPath));
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.DeletePermanentlyActionExecute(Sender: TObject);
begin
  writeln('*** Delete');
  if QuestionDlg('Delete', 'Really delete "' + ExtractFileName(SelectedPath) +
    '"?' + LineEnding + 'This can'' be undone!', mtConfirmation,
    [mrYes, 'IsDefault', mrNo], 0) = mrYes then
  try
    fLinkManager.Delete(SelectedPath);
  except
    on E: Exception do
      raise ERecoverable.Create('Error while deleting "' + SelectedPath + '"!', E);
  end;
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.CreateDirectoryActionExecute(Sender: TObject);
var
  basePath: string;
  dirNameDialog: TFilenameDialog;
  cancelled: boolean = False;
  dirName: string;
  dirPath: string;
begin
  writeln('*** Create directory');
  basePath := IncludeTrailingPathDelimiter(FileTreeGetSelectedPath);
  dirNameDialog := TFilenameDialog.Create(self,
    'Please enter the name of the directory to create.');
  dirNameDialog.NewName.Text := '';
  if dirNameDialog.ShowModal() = mrOk then
  begin
    dirName := dirNameDialog.NewName.Text;
    dirNameDialog.Free();
    dirPath := basePath + dirName;
    while TLinkManager.ExistsPath(dirPath) and not cancelled do
    begin
      if RequestNewName(dirName) then
        dirPath := basePath + dirName
      else
        cancelled := True;
    end;
    if not cancelled then
    begin
      fCommandManager.Execute(TCreateDirectoryCommand.Create(fLinkManager, dirPath));
      FileTreeUpdateView();
      UpdateUI();
    end;
  end;
end;

procedure TMainWindow.BackActionExecute(Sender: TObject);
begin
  writeln('*** Go Back');
  FileTree.Path := GetClosestExistingDirectory(fHistory.GoBack(), Root);
end;

procedure TMainWindow.ForwardActionExecute(Sender: TObject);
begin
  writeln('*** Go Forward');
  FileTree.Path := GetClosestExistingDirectory(fHistory.GoForward(), Root);
end;

procedure TMainWindow.UpActionExecute(Sender: TObject);
begin
  writeln('*** Go Up');
  FileTree.Path := GetClosestExistingDirectory(ExtractFileDir(FileTreeGetSelectedPath));
end;

procedure TMainWindow.OpenTargetActionExecute(Sender: TObject);
var
  dirPath: string;
begin
  writeln('*** Open Target');
  FileTreeUpdateView();
  if (SelectedFileInfo.FileType and ftDirectory) > 0 then
    FileTree.Path := SelectedLinkInfo.Target
  else
  begin
    dirPath := ExtractFileDir(SelectedLinkInfo.Target);
    FileTree.Path := dirPath;
    ExecuteLater(@FileListSelectSelectedLinkTarget);
  end;
  UpdateUI();
end;

procedure TMainWindow.RefreshActionExecute(Sender: TObject);
begin
  writeln('*** Refresh');
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.MoveHereActionExecute(Sender: TObject);
begin
  writeln('*** Move Here');
  fActionInfo.ActionType := atMove;
  ExecuteAction(fActionInfo);
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.CopyHereActionExecute(Sender: TObject);
begin
  writeln('*** Copy Here');
  fActionInfo.ActionType := atCopy;
  ExecuteAction(fActionInfo);
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.LinkHereRelativeActionExecute(Sender: TObject);
begin
  writeln('*** Link Here (Relative)');
  fActionInfo.ActionType := atLink;
  fActionInfo.PathType := ptRelative;
  ExecuteAction(fActionInfo);
  FileTreeUpdateView();
  UpdateUI();
end;

procedure TMainWindow.LinkHereAbsoluteActionExecute(Sender: TObject);
begin
  writeln('*** Link Here (Absolute)');
  fActionInfo.ActionType := atLink;
  fActionInfo.PathType := ptAbsolute;
  ExecuteAction(fActionInfo);
  FileTreeUpdateView();
  UpdateUI();
end;

(* --- File context menu --- *)

procedure TMainWindow.FileContextMenuPopup(Sender: TObject);
var
  clickedAt: TPoint;
  node: TTreeNode;
  item: TListItem;
  linkInfo: TLinkInfo;
begin
  clickedAt := FileList.ScreenToClient(Mouse.CursorPos);
  item := FileList.GetItemAt(clickedAt.X, clickedAt.Y);
  clickedAt := FileTree.ScreenToClient(Mouse.CursorPos);
  node := FileTree.GetNodeAt(clickedAt.X, clickedAt.Y);
  fActionInfo.Path := '';
  if Assigned(item) then
  begin
    FileList.Selected := item;
    fActionInfo.Path := FileListGetPath(item);
  end
  else if Assigned(node) then
  begin
    FileTree.Selected := node;
    if node <> FileTree.Items.GetFirstNode then
      fActionInfo.Path := FileTreeGetPath(node);
  end;
  if fActionInfo.Path <> '' then
  begin
    RenameItem.Enabled := True;
    MoveToTrashItem.Enabled := True;
    DeletePermanentlyItem.Enabled := True;
    if FileIsSymlink(fActionInfo.Path) then
    begin
      linkInfo := fLinkManager.GetLinkInfo(fActionInfo.Path);
      OpenTargetItem.Enabled := fLinkManager.IsInside(linkInfo.Target, Root);
    end
    else
      OpenTargetItem.Enabled := False;
  end
  else
  begin
    RenameItem.Enabled := False;
    MoveToTrashItem.Enabled := False;
    DeletePermanentlyItem.Enabled := False;
  end;
  CreateDirectoryItem.Enabled := FileTreeGetSelectedPath <> '';
end;

(* --- Drop popup menu --- *)

procedure TMainWindow.CanelDropItemClick(Sender: TObject);
begin
  // Do nothing
end;

(* --- Other stuff --- *)

procedure TMainWindow.MoveOrLinkIntoDirectory(const aSourcePath: string;
  const aDestinationDirectory: string);
begin
  MoveOrLinkTo(aSourcePath, IncludeTrailingPathDelimiter(aDestinationDirectory) +
    ExtractFilename(aSourcePath));
end;

procedure TMainWindow.MoveOrLinkTo(const aSourcePath: string; const aDestinationPath: string);
var
  sourceType, destinationType: integer;
begin
  with fActionInfo do
  begin
    Path := aSourcePath;
    DestinationPath := aDestinationPath;
  end;
  //TODO...
  // sourceType := TLinkManager.GetFileTypeOf(aSourcePath);
  //destinationType := TLinkManager.GetFileTypeOf(aDestinationPath);
  //MoveHereItem.Enabled := not (TLinkManager.I);
  DropPopupMenu.PopUp();
end;

function TMainWindow.ExecuteAction(var aActionInfo: TActionInfo): boolean;
var
  dirPath: string;
  destinationName: string;
  cancelled: boolean = False;
begin
  dirPath := IncludeTrailingPathDelimiter(ExtractFilePath(aActionInfo.DestinationPath));
  while TLinkManager.ExistsPath(aActionInfo.DestinationPath) and not cancelled do
  begin
    destinationName := ExtractFileName(aActionInfo.DestinationPath);
    if RequestNewName(destinationName) then
      aActionInfo.DestinationPath := dirPath + destinationName
    else
      cancelled := True;
  end;
  begin
    if cancelled then
      ExecuteAction := False
    else
    begin
      case aActionInfo.ActionType of
        atMove: MoveTo(aActionInfo.Path, aActionInfo.DestinationPath);
        atCopy: CopyTo(aActionInfo.Path, aActionInfo.DestinationPath);
        atLink: LinkAs(aActionInfo.Path, aActionInfo.DestinationPath, aActionInfo.PathType);
        else
          raise ERecoverable.Create('Unknown action!');
      end;
      ExecuteAction := True;
    end;
  end;
end;

function TMainWindow.RequestNewName(var aName: string): boolean;
var
  newNameDialog: TFilenameDialog;
begin
  newNameDialog := TFilenameDialog.Create(self, 'An item named "' + aName +
    '" already exists in the target directory. Please chose a different name.');
  newNameDialog.NewName.Text := aName;
  if newNameDialog.ShowModal() = mrOk then
  begin
    aName := newNameDialog.NewName.Text;
    RequestNewName := True;
  end
  else
    RequestNewName := False;
end;

procedure TMainWindow.MoveTo(const aSourcePath: string; const aDestinationPath: string);
begin
  fCommandManager.Execute(TMoveCommand.Create(fLinkManager, aSourcePath, aDestinationPath));
end;

procedure TMainWindow.CopyTo(const aSourcePath: string; const aDestinationPath: string);
begin
  fCommandManager.Execute(TCopyCommand.Create(fLinkManager, aSourcePath, aDestinationPath));
end;

procedure TMainWindow.LinkAs(const aLinkTargetPath: string; const aLinkedAsPath: string;
  const aPathType: TPathType);
begin
  writeln('Here!');
  fCommandManager.Execute(TLinkCommand.Create(fLinkManager, aLinkTargetPath,
    aLinkedAsPath, aPathType));
end;

procedure TMainWindow.UpdateUI();
begin
  UpdateView();
  UpdateActionExecutabilities();
end;

procedure TMainWindow.UpdateActionExecutabilities();
begin
  SetRootButton.Enabled := not Scanning;

  BackAction.Enabled := fScanSuccess and fHistory.CanGoBack;
  ForwardAction.Enabled := fScanSuccess and fHistory.CanGoForward;
  UpAction.Enabled := fScanSuccess and (FileTreeGetSelectedPath <> Root);
  OpenTargetAction.Enabled := fScanSuccess and FileIsSymlink(SelectedPath) and
    fLinkManager.IsInside(fLinkManager.GetLinkInfo(SelectedPath).Target, Root);

  UndoAction.Enabled := fScanSuccess and fCommandManager.CanUndo;
  RedoAction.Enabled := fScanSuccess and fCommandManager.CanRedo;

  RenameAction.Enabled := fScanSuccess and (fSelectedPath <> '') and (fSelectedPath <> Root);
  MoveToTrashAction.Enabled := fScanSuccess and (fSelectedPath <> '') and
    (fSelectedPath <> Root);
  DeletePermanentlyAction.Enabled :=
    fScanSuccess and (fSelectedPath <> '') and (fSelectedPath <> Root);
  CreateDirectoryAction.Enabled := fScanSuccess and (FileTreeGetSelectedPath <> '');

  RefreshAction.Enabled := fScanSuccess;

  FileTree.Enabled := fScanSuccess;
  FileList.Enabled := fScanSuccess;
end;

procedure TMainWindow.UpdateView();
begin
  StatusBarPath := fSelectedPath;
  StatusBarMessage := fCommandManager.LastActionDescription;
end;

procedure TMainWindow.ExecuteLater(aProcedure: TProc);
var
  procPtr: PProc;
begin
  New(procPtr);
  procPtr^ := aProcedure;
  Application.QueueAsyncCall(@DoExecute, PtrInt(procPtr));
end;

procedure TMainWindow.DoExecute(Data: PtrInt);
var
  procPtr: PProc;
begin
  procPtr := PProc(Data);
  try
    procPtr^();
  finally
    dispose(procPtr);
  end;
end;

function TMainWindow.GetImageIndex(const aPath: string): integer;
var
  fileType: integer;
begin
  fileType := TLinkManager.GetFileTypeOf(aPath);
  GetImageIndex := GetImageIndex(fileType);
end;

function TMainWindow.GetImageIndex(aFileType: integer): integer;
var
  imageIndex: integer;
begin
  if (aFileType and ftDirectory) = ftDirectory then
    imageIndex := 1
  else
    imageIndex := 0;
  if (aFileType and ftSymlink) = ftSymlink then
    imageIndex := imageIndex + 2;
  GetImageIndex := imageIndex;
end;

end.
