unit LinkManager;

{$mode ObjFPC}{$H+}
{$MODESWITCH ADVANCEDRECORDS}
{$modeswitch typehelpers}

interface

uses
  Classes, Generics.Collections, SysUtils;

const
  // TODO: Make configurable?
  GioPath = 'gio';
  GioTrashCommand = 'trash';
  GioTrashRestoreOption = '--restore';

  ftFile = 1;
  ftDirectory = 2;
  ftSymlink = 4;
  ftDangling = 8;

type

  ELinkManager = class(Exception);

  TUpdateLinkOperation = (ulMove, ulCopy);

  TPathType = (ptAbsolute, ptRelative);

  TPathTypeHelper = type helper for TPathType
    function ToString(): string;
  end;

  TLinkInfo = record
    Path: string;
    Target: string;
    TargetPathType: TPathType
  end;
  PLinkInfo = ^TLinkInfo;

  TLinkInfoHelper = record helper for TLinkInfo
    function ToString(): string;
  end;

  TFileType = type integer;

  TFileTypeHelper = type helper for TFileType
    function ToString(): string;
  end;

  TFileInfo = record
    Path: string;
    FileType: TFileType;
  end;

  TFileInfoHelper = record helper for TFileInfo
    function ToString(): string;
  end;

  TLinkInfoDictionary = specialize TObjectDictionary<string, PLinkInfo>;

  TLinkInfoPtrList = specialize TList<PLinkInfo>;
  TLinkInfoPtrListDictionary = specialize TObjectDictionary<string, TLinkInfoPtrList>;

  TPathList = specialize TList<string>;

  TScanFinishedHandler = procedure(const aSuccess: boolean;
    const aException: Exception) of object;

  TLinkManager = class(TObject)
  type

    TScanThread = class(TThread)
    private
      fLinkManager: TLinkManager;
      fOnScanFinished: TScanFinishedHandler;
      fSuccess: boolean;
      fException: Exception;
    protected
      procedure Execute; override;
    public
      constructor Create(const aLinkManager: TLinkManager;
        const aOnScanFinished: TScanFinishedHandler);
      procedure OnFinished(Sender: TObject);
    end;


  private
    fRoot: string;
    fLinkByPath: TLinkInfoDictionary;
    fLinksByTarget: TLinkInfoPtrListDictionary;
    fScanThread: TScanThread;
    fScanMutex: TRTLCriticalSection;
    class function CreateLinkInfo(const aLinkPath: string; const aTargetPath: string;
      const aPathType: TPathType): TLinkInfo;
    procedure SetRoot(const aValue: string);
    function GetCount: integer;
    procedure DumpLinkByPath;
    procedure DumpLinksByTarget;
    procedure Clear;
    procedure ScanPath(const aPath: string);
    procedure ScanDir(const aPath: string);
    procedure RegisterLink(const aLinkPath: string); overload;
    procedure RegisterLink(const aLinkInfo: TLinkInfo); overload;
    procedure DeletePath(const aPath: string);
    procedure DeleteDir(const aPath: string);
    procedure UnregisterPath(const aPath: string);
    procedure UnregisterDir(const aPath: string);
    procedure UnregisterLink(const aLinkInfoPtr: PLinkInfo);
    procedure MoveDirectoryTo(const aSourceInfo: TFileInfo; const aDestinationPath: string);
    procedure MoveLinkTo(const aSourceInfo: TFileInfo; const aDestinationPath: string);
    procedure MoveFileTo(const aSourceInfo: TFileInfo; const aDestinationPath: string);
    procedure UpdateMovedLinks(const aSourceInfo: TFileInfo;
      const aDestinationPath: string);
    function UpdateMovedLinkPath(const aOldPath: string; const aSourcePath: string;
      const aDestinationPath: string): PLinkInfo;
    procedure UpdateMovedLinkTargets(const aOldTargetPath: string;
      const aSourcePath: string; const aDestinationPath: string);
    procedure CopyPathTo(const aSourceBaseDir: string; const aSourcePath: string;
      const aDestinationBaseDir: string; const aDestinationPath: string);
    procedure CopyDirectoryTo(const aSourceBaseDir: string; const aSourcePath: string;
      const aDestinationBaseDir: string; const aDestinationPath: string);
    procedure CopyLinkTo(const aSourceBaseDir: string; const aSourcePath: string;
      const aDestinationBaseDir: string; const aDestinationPath: string);
    procedure CopyFileTo(const aSourcePath: string; const aDestinationPath: string);
    function GetAffectedLinkPaths(const aSourceInfo: TFileInfo): TPathList;
    function GetAffectedLinkTargetPaths(const aSourceInfo: TFileInfo): TPathList;
    procedure UpdateLinkTarget(const aLinkInfoPtr: PLinkInfo; const aTargetPath: string);
    function IsDotDir(const aName: string): boolean;
    function RebasePath(const aPath: string; const aOldBase: string; aNewBase: string): string;
    function CreateRelativePathTo(const aBaseDir: string; const aPath: string): string;
  public
    class function ExistsPath(const aPath: string): boolean;
    class function IsInside(const aPath: string; const aDir: string): boolean;
    class function GetPathTypeOf(const aPath: string): TPathType;
    class function GetFileTypeOf(const aPath: string): integer;
    class function GetFileInfo(const aPath: string): TFileInfo;
    class function GetLinkInfo(const aLinkPath: string): TLinkInfo;
    constructor Create;
    destructor Destroy; override;
    property Root: string read fRoot write setRoot;
    property Count: integer read GetCount;
    procedure Scan(const aOnScanFinished: TScanFinishedHandler = nil);
    procedure CancelScan;
    procedure MoveTo(const aSourcePath: string; const aDestinationPath: string);
    procedure CopyTo(const aSourcePath: string; const aDestinationPath: string);
    procedure LinkAs(const aLinkTargetPath: string; const aLinkedAsPath: string;
      const aPathType: TPathType);
    procedure Trash(const aPath: string);
    procedure Untrash(const aPath: string);
    procedure Delete(const aPath: string);
  end;


implementation

uses FileUtil, LazFileUtils, BaseUnix, StrUtils, Types, process, httpprotocol,
  ApplicationExceptions;

  (* --- Record and type helpers ------------------------------------- *)

  (* --- TPathTypeHelper --- *)

function TPathTypeHelper.ToString(): string;
begin
  if self = ptAbsolute then
    ToString := 'absolute'
  else
    ToString := 'relative';
end;

(* --- TLinkInfoHelper --- *)

function TLinkInfoHelper.ToString(): string;
begin
  ToString := Format('%s symlink @%p [%s -> %s]',
    [self.TargetPathType.ToString(), @self, self.Path, self.Target]);
end;

(* --- TFileTypeHelper --- *)

function TFileTypeHelper.ToString(): string;
var
  targetTypeStr: string = '';
begin
  if (self and ftSymlink) > 0 then
    targetTypeStr := 'symlink to ';
  if (self and ftFile) > 0 then
    targetTypeStr := targetTypeStr + 'file'
  else if (self and ftDirectory) > 0 then
    targetTypeStr := targetTypeStr + 'directory'
  else if (self and ftSymlink) > 0 then
    targetTypeStr := targetTypeStr + 'symlink'
  else
    targetTypeStr := targetTypeStr + 'unsupported';
  ToString := targetTypeStr;
end;

(* --- TFileInfoHelper --- *)

function TFileInfoHelper.ToString(): string;
begin
  ToString := Format('%s @%p [%s]', [self.FileType.ToString(), @self, self.Path]);
end;

(* --- Link manager ------------------------------------------------ *)

(* --- Scan thread --- *)

constructor TLinkManager.TScanThread.Create(const aLinkManager: TLinkManager;
  const aOnScanFinished: TScanFinishedHandler);
begin
  inherited Create(True);
  FreeOnTerminate := False;
  OnTerminate := @OnFinished;
  fLinkManager := aLinkManager;
  fOnScanFinished := aOnScanFinished;
  fSuccess := False;
  fException := nil;
end;

procedure TLinkManager.TScanThread.Execute;
begin
  fLinkManager.ScanPath(fLinkManager.Root);
  if Terminated then
    writeln('Scan was cancelled, ', fLinkManager.Count, ' links registered so far.')
  else
    writeln('Found ', fLinkManager.Count, ' links.');
end;

procedure TLinkManager.TScanThread.OnFinished(Sender: TObject);
var
  ex: Exception;
begin
  if Assigned(fOnScanFinished) then
  begin
    if Assigned(FatalException) then
      if FatalException is Exception then
        ex := ERecoverable.Create('Error while scanning!', Exception(FatalException))
      else
        ex := ERecoverable.Create('Error while scanning:' + LineEnding +
          FatalException.ToString())
    else
      ex := nil;
    fOnScanFinished(not (Terminated or Assigned(ex)), ex);
  end;
end;

(* --- Class methods --- *)

class function TLinkManager.ExistsPath(const aPath: string): boolean;
var
  dirPath: string;
  targetName: string;
  entryInfo: TSearchRec;
begin
  ExistsPath := False;
  dirPath := ExtractFilePath(aPath);
  targetName := ExtractFileName(aPath);
  if FindFirst(dirPath + '*', faAnyFile, entryInfo) = 0 then
  try
    repeat
      if entryInfo.Name = targetName then
      begin
        ExistsPath := True;
        break;
      end;
    until FindNext(entryInfo) <> 0;
  finally
    FindClose(entryInfo);
  end
  else
    raise ELinkManager.Create('Error reading directory "' + dirPath + '"!');
end;

class function TLinkManager.IsInside(const aPath: string; const aDir: string): boolean;
begin
  IsInside := FileIsInPath(aPath, aDir);
end;

class function TLinkManager.GetFileTypeOf(const aPath: string): integer;
var
  fileType: integer = 0;
  stat: TStat;
begin
  if DirectoryExists(aPath) then
    fileType := ftDirectory
  else
  if fpStat(aPath, stat) = 0 then
    if fpS_ISREG(stat.st_mode) then
      fileType := ftFile
    else
      raise ELinkManager.Create('Error reading file info for "' + aPath + '"!');
  if FileIsSymlink(aPath) then
    fileType := fileType or ftSymlink;
  GetFileTypeOf := fileType;
end;

class function TLinkManager.GetPathTypeOf(const aPath: string): TPathType;
begin
  if FilenameIsAbsolute(aPath) then
    GetPathTypeOf := ptAbsolute
  else
    GetPathTypeOf := ptRelative;
end;

class function TLinkManager.GetFileInfo(const aPath: string): TFileInfo;
var
  targetInfo: TFileInfo;
begin
  with targetInfo do
  begin
    Path := aPath;
    FileType := GetFileTypeOf(aPath);
  end;
  GetFileInfo := targetInfo;
end;

class function TLinkManager.GetLinkInfo(const aLinkPath: string): TLinkInfo;
var
  linkDir: string;
  linkedPath: string;
  targetPath: string;
begin
  linkDir := ExtractFileDir(aLinkPath);
  linkedPath := fpReadLink(aLinkPath);
  if linkedPath <> '' then
    targetPath := CreateAbsolutePath(linkedPath, linkDir)
  else
    targetPath := '';
  GetLinkInfo := CreateLinkInfo(aLinkPath, targetPath, GetPathTypeOf(linkedPath));
end;


class function TLinkManager.CreateLinkInfo(const aLinkPath: string;
  const aTargetPath: string; const aPathType: TPathType): TLinkInfo;
var
  linkInfo: TLinkInfo;
begin
  with linkInfo do
  begin
    Path := aLinkPath;
    Target := aTargetPath;
    TargetPathType := aPathType;
  end;
  CreateLinkInfo := linkInfo;
end;

(* --- Construction / destruction --- *)

constructor TLinkManager.Create();
begin
  fLinkByPath := TLinkInfoDictionary.Create();
  fLinksByTarget := TLinkInfoPtrListDictionary.Create();
  InitCriticalSection(fScanMutex);
end;

destructor TLinkManager.Destroy();
begin
  Clear();
  fLinkByPath.Free();
  fLinksByTarget.Free();
  inherited;
end;

(* --- Getters / setters --- *)

procedure TLinkManager.SetRoot(const aValue: string);
begin
  fRoot := aValue;
  writeln('Root set to ' + fRoot);
end;

function TLinkManager.GetCount(): integer;
begin
  GetCount := fLinkByPath.Count;
end;


(* --- Other methods --- *)

procedure TLinkManager.DumpLinkByPath();
var
  path: string;
  linkInfoPtr: PLinkInfo;
begin
  writeln('linkByPath = {');
  for path in fLinkByPath.Keys do
  begin
    linkInfoPtr := fLinkByPath[path];
    writeln('  ' + path + ': ' + linkInfoPtr^.ToString());
  end;
  writeln('}');
end;

procedure TLinkManager.DumpLinksByTarget();
var
  targetPath: string;
  linkInfoPtrs: TLinkInfoPtrList;
  linkInfoPtr: PLinkInfo;
begin
  writeln('linksByTarget = {');
  for targetPath in fLinksByTarget.Keys do
  begin
    writeln('  ' + targetPath + ': [');
    linkInfoPtrs := fLinksByTarget[targetPath];
    for linkInfoPtr in linkInfoPtrs do
      writeln('    ' + linkInfoPtr^.ToString() + ',');
    writeln('  ],');
  end;
  writeln('}');
end;

procedure TLinkManager.Clear();
var
  linkInfoPtrs: TLinkInfoPtrList;
  linkInfoPtr: PLinkInfo;
begin
  writeln('Clearing ' + fLinkByPath.Count.ToString() + ' links...');

  EnterCriticalSection(fScanMutex);
  try
    if Assigned(fScanThread) then
    begin
      fScanThread.Terminate();
      fScanThread.WaitFor();
      fScanThread.Free();
    end;

    linkInfoPtrs := TLinkInfoPtrList.Create(fLinkByPath.Values);
    for linkInfoPtr in linkInfoPtrs do
    begin
      UnregisterLink(linkInfoPtr);
      //writeln('  ', fLinkByPath.Count.ToString(), ' links remaining.');
    end;
    linkInfoPtrs.Clear;
    linkInfoPtrs.Free;
  finally
    LeaveCriticalSection(fScanMutex);
  end;
end;

procedure TLinkManager.Scan(const aOnScanFinished: TScanFinishedHandler = nil);
begin
  EnterCriticalSection(fScanMutex);
  try
    Clear();
    writeln('Starting scan...');
    if Assigned(aOnScanFinished) then
    begin
      fScanThread := TScanThread.Create(self, aOnScanFinished);
      fScanThread.Start();
    end
    else
    begin
      fScanThread := nil;
      ScanPath(Root);
    end;
  finally
    LeaveCriticalSection(fScanMutex);
  end;
end;

procedure TLinkManager.CancelScan;
begin
  EnterCriticalSection(fScanMutex);
  try
    if Assigned(fScanThread) then
    begin
      writeln('Cancelling current scan...');
      fScanThread.Terminate;
    end;
  finally
    LeaveCriticalSection(fScanMutex);
  end;
end;

procedure TLinkManager.ScanPath(const aPath: string);
begin
  //writeln('Processing ' + aPath);
  if FileIsSymlink(aPath) then
    RegisterLink(aPath)
  else if DirectoryExists(aPath) then
    ScanDir(aPath);
end;

procedure TLinkManager.ScanDir(const aPath: string);
var
  dirPath: string;
  entryInfo: TSearchRec;
begin
  dirPath := IncludeTrailingPathDelimiter(aPath);
  writeln('Scanning directory ' + aPath);
  if FindFirst(dirPath + '*', faAnyFile, entryInfo) = 0 then
  try
    repeat
      if not IsDotDir(entryInfo.Name) then
        ScanPath(dirPath + entryInfo.Name)
    until (FindNext(entryInfo) <> 0) or (Assigned(fScanThread) and fScanThread.Terminated);
  finally
    FindClose(entryInfo);
  end
  else
    raise ELinkManager.Create('Error scanning directory "' + aPath + '"!');
end;

procedure TLinkManager.RegisterLink(const aLinkPath: string);
var
  linkInfo: TLinkInfo;
begin
  linkInfo := GetLinkInfo(aLinkPath);
  if linkInfo.Target <> '' then
    RegisterLink(linkInfo)
  else
    writeln('Warning: ' + linkInfo.ToString() + ' is dangling!');
end;

procedure TLinkManager.RegisterLink(const aLinkInfo: TLinkInfo);
var
  targetLinkPtrs: TLinkInfoPtrList;
  linkInfoPtr: PLinkInfo;
begin
  if not fLinkByPath.ContainsKey(aLinkInfo.Path) then
  begin
    writeln('  Registering ' + aLinkInfo.ToString());

    new(linkInfoPtr);
    linkInfoPtr^ := aLinkInfo;
    fLinkByPath.Add(linkInfoPtr^.Path, linkInfoPtr);

    if fLinksByTarget.ContainsKey(linkInfoPtr^.Target) then
      targetLinkPtrs := fLinksByTarget.Items[linkInfoPtr^.Target]
    else
    begin
      targetLinkPtrs := TLinkInfoPtrList.Create();
      fLinksByTarget.Add(linkInfoPtr^.Target, targetLinkPtrs);
    end;
    targetLinkPtrs.Add(linkInfoPtr);
    //  writeln('    ' + fLinkByPath.Count.ToString + ' links registered.');
  end;
end;

procedure TLinkManager.DeletePath(const aPath: string);
var
  success: boolean = True;
begin
  if fLinkByPath.ContainsKey(aPath) then begin
    UnregisterLink(fLinkByPath[aPath]);
    success := DeleteFile(aPath);
  end
  else
    if DirectoryExists(aPath) then
    begin
      DeleteDir(aPath);
      success := DeleteDirectory(aPath, False);
    end
    else
      success := DeleteFile(aPath);
  if not success then
    raise ELinkManager.Create('Error deleting "' + aPath + '"!');
end;

procedure TLinkManager.DeleteDir(const aPath: string);
var
  dirPath: string;
  entryInfo: TSearchRec;
begin
  dirPath := IncludeTrailingPathDelimiter(aPath);
  writeln('Deleting directory ' + aPath);
  if FindFirst(dirPath + '*', faAnyFile, entryInfo) = 0 then
  try
    repeat
      if not IsDotDir(entryInfo.Name) then
        DeletePath(dirPath + entryInfo.Name)
    until (FindNext(entryInfo) <> 0);
  finally
    FindClose(entryInfo);
  end
  else
    raise ELinkManager.Create('Error deleting directory "' + aPath + '"!');
end;

procedure TLinkManager.UnregisterPath(const aPath: string);
begin
  if fLinkByPath.ContainsKey(aPath) then
    UnregisterLink(fLinkByPath[aPath])
  else if DirectoryExists(aPath) then
    UnregisterDir(aPath);
end;

procedure TLinkManager.UnregisterDir(const aPath: string);
var
  dirPath: string;
  entryInfo: TSearchRec;
begin
  dirPath := IncludeTrailingPathDelimiter(aPath);
  writeln('Unregistering directory ' + aPath);
  if FindFirst(dirPath + '*', faAnyFile, entryInfo) = 0 then
  try
    repeat
      if not IsDotDir(entryInfo.Name) then
        UnregisterPath(dirPath + entryInfo.Name)
    until (FindNext(entryInfo) <> 0);
  finally
    FindClose(entryInfo);
  end
  else
    raise ELinkManager.Create('Error unregistering directory "' + aPath + '"!');
end;

procedure TLinkManager.UnregisterLink(const aLinkInfoPtr: PLinkInfo);
var
  linkInfoPtrs: TLinkInfoPtrList;
begin
  if fLinkByPath.ContainsKey(aLinkInfoPtr^.Path) then
  begin
    writeln('  Unregistering ' + aLinkInfoPtr^.ToString());
    fLinkByPath.Remove(aLinkInfoPtr^.Path);
    linkInfoPtrs := fLinksByTarget.Items[aLinkInfoPtr^.Target];
    linkInfoPtrs.Remove(aLinkInfoPtr);
    if linkInfoPtrs.Count = 0 then
    begin
      fLinksByTarget.Remove(aLinkInfoPtr^.Target);
      linkInfoPtrs.Free;
    end;
    dispose(aLinkInfoPtr);
  end;
end;

procedure TLinkManager.MoveTo(const aSourcePath: string; const aDestinationPath: string);
var
  sourceInfo: TFileInfo;
begin
  writeln('Moving ' + aSourcePath + ' => ' + aDestinationPath);
  {$ifopt D+}
  DumpLinkByPath();
  DumpLinksByTarget();
  {$EndIf}

  sourceInfo := GetFileInfo(aSourcePath);

  if (sourceInfo.FileType and ftSymlink) > 0 then
    MoveLinkTo(sourceInfo, aDestinationPath)
  else if (sourceInfo.FileType and ftDirectory) > 0 then
    MoveDirectoryTo(sourceInfo, aDestinationPath)
  else
    MoveFileTo(sourceInfo, aDestinationPath);

  UpdateMovedLinks(sourceInfo, aDestinationPath);

  {$ifopt D+}
  DumpLinkByPath();
  DumpLinksByTarget();
  {$EndIf}
end;

procedure TLinkManager.MoveDirectoryTo(const aSourceInfo: TFileInfo;
  const aDestinationPath: string);
begin
  writeln('  Moving directory ' + aSourceInfo.Path + ' => ' + aDestinationPath);
  if not RenameFile(IncludeTrailingPathDelimiter(aSourceInfo.Path),
    IncludeTrailingPathDelimiter(aDestinationPath)) then
    raise ELinkManager.Create('Error moving/renaming directory "' +
      aSourceInfo.Path + '" to "' + aDestinationPath + '"!');
end;

procedure TLinkManager.MoveLinkTo(const aSourceInfo: TFileInfo;
  const aDestinationPath: string);
var
  linkInfoPtr: PLinkInfo;
begin
  writeln('  Moving symlink ' + aSourceInfo.Path + ' => ' + aDestinationPath);
  if not fLinkByPath.ContainsKey(aSourceInfo.Path) then
    RegisterLink(aSourceInfo.Path);
  if RenameFile(aSourceInfo.Path, aDestinationPath) then
  begin
    linkInfoPtr := fLinkByPath[aSourceInfo.Path];
    fLinkByPath.Remove(aSourceInfo.Path);
    linkInfoPtr^.Path := aDestinationPath;
    fLinkByPath.Add(aDestinationPath, linkInfoPtr);
    UpdateLinkTarget(linkInfoPtr, linkInfoPtr^.Target);
  end
  else
    raise ELinkManager.Create('Error moving/renaming symlink "' +
      aSourceInfo.Path + '" to "' + aDestinationPath + '"!');
end;

procedure TLinkManager.MoveFileTo(const aSourceInfo: TFileInfo;
  const aDestinationPath: string);
begin
  writeln('  Moving file ' + aSourceInfo.Path + ' => ' + aDestinationPath);
  if not RenameFile(aSourceInfo.Path, aDestinationPath) then
    raise ELinkManager.Create('Error moving/renaming file "' + aSourceInfo.Path +
      '" to "' + aDestinationPath + '"!');
end;

procedure TLinkManager.UpdateMovedLinks(const aSourceInfo: TFileInfo;
  const aDestinationPath: string);
var
  affectedPaths: TPathList;
  linkInfoPtr: PLinkInfo;
  oldPath: string;
begin
  affectedPaths := GetAffectedLinkPaths(aSourceInfo);
  for oldPath in affectedPaths do begin
    linkInfoPtr := UpdateMovedLinkPath(oldPath, aSourceInfo.Path, aDestinationPath);
    UpdateLinkTarget(linkInfoPtr, linkInfoPtr^.Target);
  end;
  affectedPaths.Free();

  affectedPaths := GetAffectedLinkTargetPaths(aSourceInfo);
  for oldPath in affectedPaths do
    UpdateMovedLinkTargets(oldPath, aSourceInfo.Path, aDestinationPath);
  affectedPaths.Free();
end;


function TLinkManager.UpdateMovedLinkPath(const aOldPath: string;
  const aSourcePath: string; const aDestinationPath: string): PLinkInfo;
var
  linkInfoPtr: PLinkInfo;
  newPath: string;
begin
  linkInfoPtr := fLinkByPath[aOldPath];
  writeln('    Updating path to ' + linkInfoPtr^.ToString());
  fLinkByPath.Remove(aOldPath);
  newPath := RebasePath(aOldPath, aSourcePath, aDestinationPath);
  linkInfoPtr^.Path := newPath;
  fLinkByPath.Add(newPath, linkInfoPtr);
  writeln('    => ' + linkInfoPtr^.ToString());
  UpdateMovedLinkPath:= linkInfoPtr;
end;

procedure TLinkManager.UpdateMovedLinkTargets(const aOldTargetPath: string;
  const aSourcePath: string; const aDestinationPath: string);
var
  linkInfoPtrs: TLinkInfoPtrList;
  linkInfoPtr: PLinkInfo;
  newTargetPath: string;
begin
  linkInfoPtrs := fLinksByTarget[aOldTargetPath];
  fLinksByTarget.Remove(aOldTargetPath);

  newTargetPath := RebasePath(aOldTargetPath, aSourcePath, aDestinationPath);
  for linkInfoPtr in linkInfoPtrs do
    UpdateLinkTarget(linkInfoPtr, newTargetPath);

  fLinksByTarget.Add(newTargetPath, linkInfoPtrs);
end;

procedure TLinkManager.CopyTo(const aSourcePath: string; const aDestinationPath: string);
var
  sourceInfo: TFileInfo;
  sourceBaseDir: string;
  destinationBaseDir: string;
begin
  writeln('Copying ' + aSourcePath + ' => ' + aDestinationPath);
  {$ifopt D+}
  DumpLinkByPath();
  DumpLinksByTarget();
  {$EndIf}

  sourceInfo := GetFileInfo(aSourcePath);
  if (sourceInfo.FileType and ftDirectory) > 0 then
  begin
    sourceBaseDir := aSourcePath;
    destinationBaseDir := aDestinationPath;
  end
  else
  begin
    sourceBaseDir := ExtractFileDir(aSourcePath);
    destinationBaseDir := ExtractFileDir(aDestinationPath);
  end;

  CopyPathTo(sourceBaseDir, aSourcePath, destinationBaseDir, aDestinationPath);
  ScanPath(aDestinationPath);

  {$ifopt D+}
  DumpLinkByPath();
  DumpLinksByTarget();
  {$EndIf}
end;

procedure TLinkManager.CopyPathTo(const aSourceBaseDir: string;
  const aSourcePath: string; const aDestinationBaseDir: string;
  const aDestinationPath: string);
var
  sourceInfo: TFileInfo;
begin
  sourceInfo := GetFileInfo(aSourcePath);

  if (sourceInfo.FileType and ftSymlink) > 0 then
    CopyLinkTo(aSourceBaseDir, aSourcePath, aDestinationBaseDir, aDestinationPath)
  else if (sourceInfo.FileType and ftDirectory) > 0 then
    CopyDirectoryTo(aSourceBaseDir, aSourcePath, aDestinationBaseDir, aDestinationPath)
  else
    CopyFileTo(aSourcePath, aDestinationPath);
end;

procedure TLinkManager.CopyDirectoryTo(const aSourceBaseDir: string;
  const aSourcePath: string; const aDestinationBaseDir: string;
  const aDestinationPath: string);
var
  sourceDirPath: string;
  destinationDirPath: string;
  entryInfo: TSearchRec;
begin
  writeln('  Copying directory ' + aSourcePath + ' => ' + aDestinationPath);
  sourceDirPath := IncludeTrailingPathDelimiter(aSourcePath);
  destinationDirPath := IncludeTrailingPathDelimiter(aDestinationPath);
  CreateDir(aDestinationPath);
  if FindFirst(sourceDirPath + '*', faAnyFile, entryInfo) = 0 then
  try
    repeat
      if not IsDotDir(entryInfo.Name) then
        CopyPathTo(aSourceBaseDir, sourceDirPath + entryInfo.Name,
          aDestinationBaseDir, destinationDirPath + entryInfo.Name);
    until (FindNext(entryInfo) <> 0);
  finally
    FindClose(entryInfo);
  end
  else
    raise ELinkManager.Create('Error copying directory "' + aSourcePath + '"!');
end;


procedure TLinkManager.CopyLinkTo(const aSourceBaseDir: string;
  const aSourcePath: string; const aDestinationBaseDir: string;
  const aDestinationPath: string);
var
  oldLinkInfo: TLinkInfo;
  newlinkInfoPtr: PLinkInfo;
  targetPath: string;
begin
  writeln('  Copying symlink ' + aSourcePath + ' => ' + aDestinationPath);
  oldLinkInfo := GetLinkInfo(aSourcePath);

  new(newLinkInfoPtr);
  newLinkInfoPtr^.Path := aDestinationPath;
  if IsInside(oldLinkInfo.Target, aSourceBaseDir) then
    newLinkInfoPtr^.Target := RebasePath(oldLinkInfo.Target, aSourceBaseDir,
      aDestinationBaseDir)
  else
    newLinkInfoPtr^.Target := oldLinkInfo.Target;
  newLinkInfoPtr^.TargetPathType := oldLinkInfo.TargetPathType;

  if newlinkInfoPtr^.TargetPathType = ptRelative then
    targetPath := CreateRelativePathTo(ExtractFileDir(newlinkInfoPtr^.Path),
      newlinkInfoPtr^.Target)
  else
    targetPath := newlinkInfoPtr^.Target;

  if fpSymlink(PChar(targetPath), PChar(newLinkInfoPtr^.Path)) <> 0 then
    raise ELinkManager.Create('Error creating symlink "' + newLinkInfoPtr^.Path +
      '" -> "' + targetPath + '"!');

  writeln('    => ' + newLinkInfoPtr^.ToString());
  dispose(newLinkInfoPtr);
end;

procedure TLinkManager.CopyFileTo(const aSourcePath: string; const aDestinationPath: string);
begin
  writeln('  Copying file ' + aSourcePath + ' => ' + aDestinationPath);
  if not CopyFile(aSourcePath, aDestinationPath, True) then
    raise ELinkManager.Create('Error copying file "' + aSourcePath +
      '" to "' + aDestinationPath + '"!');
end;

function TLinkManager.GetAffectedLinkPaths(const aSourceInfo: TFileInfo): TPathList;
var
  paths: TPathList;
  path: string;
begin
  paths := TPathList.Create();
  for path in fLinkByPath.Keys do
    if IsInside(path, aSourceInfo.Path) then
      paths.add(path);
  GetAffectedLinkPaths := paths;
end;

function TLinkManager.GetAffectedLinkTargetPaths(const aSourceInfo: TFileInfo): TPathList;
var
  paths: TPathList;
  path: string;
begin
  paths := TPathList.Create();
  if fLinksByTarget.ContainsKey(aSourceInfo.Path) then
    paths.Add(aSourceInfo.Path);
  if (aSourceInfo.FileType and ftDirectory) > 0 then
    for path in fLinksByTarget.Keys do
      if IsInside(path, aSourceInfo.Path) then
        paths.Add(path);
  GetAffectedLinkTargetPaths := paths;
end;

procedure TLinkManager.UpdateLinkTarget(const aLinkInfoPtr: PLinkInfo;
  const aTargetPath: string);
var
  targetPath: string;
begin
  writeln('    Updating target of ' + aLinkInfoPtr^.ToString());
  if aLinkInfoPtr^.TargetPathType = ptAbsolute then
    targetPath := aTargetPath
  else
    targetPath := CreateRelativePathTo(ExtractFileDir(aLinkInfoPtr^.Path), aTargetPath);
  if DeleteFile(aLinkInfoPtr^.Path) then
    if fpSymlink(PChar(targetPath), PChar(aLinkInfoPtr^.Path)) <> 0 then
      raise ELinkManager.Create('Error creating symlink "' + aLinkInfoPtr^.Path +
        '" -> "' + aTargetPath + '"!')
    else
    begin
      // Do nothing
    end
  else
    raise ELinkManager.Create('Error deleting symlink "' + aLinkInfoPtr^.Path + '"!');
  aLinkInfoPtr^.Target := aTargetPath;
  writeln('    => ' + aLinkInfoPtr^.ToString());
end;

procedure TLinkManager.LinkAs(const aLinkTargetPath: string; const aLinkedAsPath: string;
  const aPathType: TPathType);
var
  linkDir: string;
  targetPath: string;
begin
  writeln('Linking ' + aLinkTargetPath + ' <- ' + aLinkedAsPath);
  if aPathType = ptAbsolute then
    targetPath := aLinkTargetPath
  else
  begin
    linkDir := ExtractFileDir(aLinkedAsPath);
    targetPath := CreateRelativePathTo(linkDir, aLinkTargetPath);
  end;
  if fpSymlink(PChar(targetPath), PChar(aLinkedAsPath)) = 0 then
    RegisterLink(CreateLinkInfo(aLinkedAsPath, aLinkTargetPath, aPathType))
  else
    raise ELinkManager.Create('Error creating symlink "' + aLinkedAsPath +
      '" -> "' + aLinkTargetPath + '"!');
end;

procedure TLinkManager.Delete(const aPath: string);
begin
  DeletePath(aPath);
end;

procedure TLinkManager.Trash(const aPath: string);
var
  outStr: string;
begin
  UnregisterPath(aPath);
  if not RunCommand(GioPath, [GioTrashCommand, aPath], outStr) then
    raise ELinkManager.Create('Error moving "' + aPath + '" to trash: ' + outStr);
end;

procedure TLinkManager.Untrash(const aPath: string);
var
  url: string;
  outStr: string;
begin
  url := 'trash:///' + HTTPEncode(ExtractFileName(aPath));
  if not RunCommand(GioPath, [GioTrashCommand, GioTrashRestoreOption, url], outStr) then
    raise ELinkManager.Create('Error restoring "' + aPath + '" from trash: ' + outStr);
  ScanPath(aPath);
end;

function TLinkManager.IsDotDir(const aName: string): boolean;
begin
  if (aName = '.') or (aName = '..') then
    IsDotDir := True
  else
    IsDotDir := False;
end;

function TLinkManager.RebasePath(const aPath: string; const aOldBase: string;
  aNewBase: string): string;
var
  relPath: string;
begin
  relPath := ExcludeTrailingPathDelimiter(CreateRelativePath(aPath, aOldBase));
  RebasePath := ExcludeTrailingPathDelimiter(IncludeTrailingPathDelimiter(aNewBase) + relPath);
end;

function TLinkManager.CreateRelativePathTo(const aBaseDir: string;
  const aPath: string): string;
var
  pathSegments, baseSegments: TStringDynArray;
  commonPrefixLength: integer = 0;
  i: integer;
  relPath: string;
begin
  pathSegments := SplitString(ExtractFileDir(aPath), DirectorySeparator);
  baseSegments := SplitString(aBaseDir, DirectorySeparator);
  while (commonPrefixLength < Length(pathSegments)) and
    (commonPrefixLength < Length(baseSegments)) and
    (pathSegments[commonPrefixLength] = baseSegments[commonPrefixLength]) do
    Inc(commonPrefixLength);
  relPath := '';
  for i := (commonPrefixLength) to Length(baseSegments) - 1 do
    relPath := relPath + '..' + DirectorySeparator;
  for i := commonPrefixLength to Length(pathSegments) - 1 do
    relPath := relPath + pathSegments[i] + DirectorySeparator;
  relPath := relPath + ExtractFileName(aPath);
  CreateRelativePathTo := relPath;
end;

end.
