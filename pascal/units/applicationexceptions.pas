unit ApplicationExceptions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type

  EApplication = class abstract(Exception)
  public
    constructor Create(const aMessage: string; const aCause: Exception); overload;
  end;

  ERecoverable = class(EApplication)
  public
    constructor Create(const aCause: Exception);
    constructor Create(const aMessage: string; const aCause: Exception); overload;
  end;

  EFatal = class(EApplication)
  public
    constructor Create(const aCause: Exception);
    constructor Create(const aMessage: string; const aCause: Exception); overload;
  end;


implementation

constructor EApplication.Create(const aMessage: string; const aCause: Exception);
begin
  inherited Create(aMessage + LineEnding + 'Caused by:' + LineEnding + aCause.Message);
end;

constructor ERecoverable.Create(const aCause: Exception);
begin
  inherited Create('A recoverable error has occured:' + LineEnding + aCause.Message);
end;

constructor ERecoverable.Create(const aMessage: string; const aCause: Exception); overload;
begin
  inherited Create(aMessage, aCause);
end;

constructor EFatal.Create(const aCause: Exception);
begin
  inherited Create('A fatal error has occured:' + LineEnding + aCause.Message + LineEnding + 'This application will be terminated!');
end;

constructor EFatal.Create(const aMessage: string; const aCause: Exception); overload;
begin
  inherited Create(aMessage, aCause);
end;

end.

