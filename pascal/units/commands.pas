unit Commands;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,
  LinkManager, CommandManager;

type

  TRevertableCommand = class abstract(TInterfacedObject, ICommand)
  private
    fDescription: string;
  protected
    constructor Create(const aDescription: string);
  public
    function IsRevertable: boolean;
    function GetDescription: string;
    property Revertable: boolean read IsRevertable;
    property Description: string read GetDescription;
    function Execute(): boolean; virtual;
    function Unexecute(): boolean; virtual;
    function DoExecute(): boolean; virtual; abstract;
    function DoUnexecute(): boolean; virtual; abstract;
  end;

  TLinkManagerCommand = class abstract(TRevertableCommand)
  private
    fLinkManager: TLinkManager;
    function GetLinkManager: TLinkManager;
  protected
    constructor Create(const aDescription: string; const aLinkManager: TLinkManager);
    property LinkManager: TLinkManager read GetLinkManager;
  end;

  TMoveCommand = class(TLinkManagerCommand)
  private
    fSourcePath: string;
    fDestinationPath: string;
  public
    constructor Create(const aLinkManager: TLinkManager;
      const aSourcePath, aDestinationPath: string);
    function DoExecute(): boolean; override;
    function DoUnexecute(): boolean; override;
  end;

  TCopyCommand = class(TLinkManagerCommand)
  private
    fSourcePath: string;
    fDestinationPath: string;
  public
    constructor Create(const aLinkManager: TLinkManager;
      const aSourcePath, aDestinationPath: string);
    function DoExecute(): boolean; override;
    function DoUnexecute(): boolean; override;
  end;

  TLinkCommand = class(TLinkManagerCommand)
  private
    fLinkTargetPath: string;
    fLinkedAsPath: string;
    fPathType: TPathType;
  public
    constructor Create(const aLinkManager: TLinkManager;
      const aLinkTargetPath, aLinkedAsPath: string; aPathType: TPathType);
    function DoExecute(): boolean; override;
    function DoUnexecute(): boolean; override;
  end;

  TTrashCommand = class(TLinkManagerCommand)
  private
    fPath: string;
  public
    constructor Create(const aLinkManager: TLinkManager; const aPath: string);
    function DoExecute(): boolean; override;
    function DoUnexecute(): boolean; override;
  end;

  TCreateDirectoryCommand = class(TLinkManagerCommand)
  private
    fPath: string;
  public
    constructor Create(const aLinkManager: TLinkManager; const aPath: string);
    function DoExecute(): boolean; override;
    function DoUnexecute(): boolean; override;
  end;

implementation

uses
  FileUtil, Dialogs, Controls,
  ApplicationExceptions;

  (* --- TRevertableCommand ------------------------------------------ *)

constructor TRevertableCommand.Create(const aDescription: string);
begin
  fDescription := aDescription;
end;

function TRevertableCommand.IsRevertable: boolean;
begin
  IsRevertable := True;
end;

function TRevertableCommand.GetDescription: string;
begin
  GetDescription := fDescription;
end;

function TRevertableCommand.Execute: boolean;
begin
  try
    Execute := DoExecute();
  except
    on E: Exception do
      raise ERecoverable.Create('Error while executing ' + Description, E)
  end;
end;

function TRevertableCommand.Unexecute: boolean;
begin
  try
    Unexecute := DoUnexecute();
  except
    on E: Exception do
      raise ERecoverable.Create('Error while reverting ' + Description, E)
  end;
end;

(* --- TLinkManagerCommand ----------------------------------------- *)

constructor TLinkManagerCommand.Create(const aDescription: string;
  const aLinkManager: TLinkManager);
begin
  inherited Create(aDescription);
  fLinkManager := aLinkManager;
end;

function TLinkManagerCommand.GetLinkManager: TLinkManager;
begin
  GetLinkManager := fLinkManager;
end;

(* --- TMoveCommand ------------------------------------------------ *)

constructor TMoveCommand.Create(const aLinkManager: TLinkManager;
  const aSourcePath, aDestinationPath: string);
begin
  inherited Create('Move/Rename ' + ExtractFileName(aSourcePath), aLinkManager);
  fLinkManager := aLinkManager;
  fSourcePath := aSourcePath;
  fDestinationPath := aDestinationPath;
end;

function TMoveCommand.DoExecute(): boolean;
begin
  LinkManager.MoveTo(fSourcePath, fDestinationPath);
  DoExecute := True;
end;

function TMoveCommand.DoUnexecute(): boolean;
begin
  LinkManager.MoveTo(fDestinationPath, fSourcePath);
  DoUnexecute := True;
end;

(* --- TCopyCommand ------------------------------------------------ *)

constructor TCopyCommand.Create(const aLinkManager: TLinkManager;
  const aSourcePath, aDestinationPath: string);
begin
  inherited Create('Copy ' + ExtractFileName(aSourcePath), aLinkManager);
  fLinkManager := aLinkManager;
  fSourcePath := aSourcePath;
  fDestinationPath := aDestinationPath;
end;

function TCopyCommand.DoExecute(): boolean;
begin
  LinkManager.CopyTo(fSourcePath, fDestinationPath);
  DoExecute := True;
end;

function TCopyCommand.DoUnexecute(): boolean;
begin
  if QuestionDlg('Undo', 'This will move "' + ExtractFileName(fDestinationPath) +
    '" to trash. Continue?', mtConfirmation, [mrYes, 'IsDefault', mrNo], 0) = mrYes then
  begin
    fLinkManager.Trash(fDestinationPath);
    DoUnexecute := True;
  end
  else
    DoUnexecute := False;
end;

(* --- TLinkCommand ------------------------------------------------ *)

constructor TLinkCommand.Create(const aLinkManager: TLinkManager;
  const aLinkTargetPath, aLinkedAsPath: string; aPathType: TPathType);
begin
  inherited Create('Link ' + ExtractFileName(aLinkTargetPath), aLinkManager);
  fLinkTargetPath := aLinkTargetPath;
  fLinkedAsPath := aLinkedAsPath;
  fPathType := aPathType;
end;


function TLinkCommand.DoExecute(): boolean;
begin
  LinkManager.LinkAs(fLinkTargetPath, fLinkedAsPath, fPathType);
  DoExecute := True;
end;

function TLinkCommand.DoUnexecute(): boolean;
begin
  if (LinkManager.GetFileTypeOf(fLinkedAsPath) and ftSymlink) > 0 then
    LinkManager.Delete(fLinkedAsPath);
  DoUnexecute := True;
end;

(* --- TTrashCommand ----------------------------------------------- *)

constructor TTrashCommand.Create(const aLinkManager: TLinkManager; const aPath: string);
begin
  inherited Create('Trash ' + ExtractFileName(aPath), aLinkManager);
  fPath := aPath;
end;


function TTrashCommand.DoExecute(): boolean;
begin
  fLinkManager.Trash(fPath);
  DoExecute := True;
end;

function TTrashCommand.DoUnexecute(): boolean;
begin
  fLinkManager.Untrash(fPath);
  DoUnexecute := True;
end;


(* --- TTrashCommand ----------------------------------------------- *)

constructor TCreateDirectoryCommand.Create(const aLinkManager: TLinkManager;
  const aPath: string);
begin
  inherited Create('Create directory ' + ExtractFileName(aPath), aLinkManager);
  fPath := aPath;
end;

function TCreateDirectoryCommand.DoExecute(): boolean;
begin
  if fLinkManager.ExistsPath(fPath) then
    raise ECommand.Create('"' + fPath + '" already exists!')
  else
  if not CreateDir(fPath) then
    raise ECommand.Create('Error creating directory "' + fPath + '"!');
  DoExecute := True;
end;

function TCreateDirectoryCommand.DoUnexecute(): boolean;
begin
  if QuestionDlg('Undo', 'This will move directory "' + ExtractFileName(fPath) +
    '" to trash. Continue?', mtConfirmation, [mrYes, 'IsDefault', mrNo], 0) = mrYes then
  begin
    fLinkManager.Trash(fPath);
    DoUnexecute := True;
  end
  else
    DoUnexecute := False;
end;

end.
