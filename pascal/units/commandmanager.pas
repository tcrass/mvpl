unit CommandManager;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Generics.Collections,
  HistoryManager;

type

  ECommand = class(Exception);

  ICommand = interface
    function GetDescription: string;
    property Revertable: boolean;
    property Description: string read GetDescription;
    function Execute(): boolean;
    function Unexecute(): boolean;
  end;

  TCommandList = specialize TList<ICommand>;

  ICommandManager = interface
    property CanUndo: boolean;
    property CanRedo: boolean;
    property Current: ICommand;
    property LastActionDescription: string;
    procedure Execute(aCommand: ICommand);
    procedure Undo();
    procedure Redo();
    procedure Clear();
  end;

  TCommandManager = class(TInterfacedObject, ICommandManager)
  type
    TCommands = specialize THistoryManager<ICommand>;


  private
    fCommands: TCommands;
    fLastActionDescription: string;
    procedure SetLastActionDescription(const aDescription: string);
    function GetCanUndo: boolean;
    function GetCanRedo: boolean;
    function GetCurrent: ICommand;
    function GetLastActionDescription: string;
  public
    property CanUndo: boolean read GetCanUndo;
    property CanRedo: boolean read GetCanRedo;
    property Current: ICommand read GetCurrent;
    property LastActionDescription: string read GetLastActionDescription
      write SetLastActionDescription;
    constructor Create();
    destructor Destroy(); override;
    procedure Execute(aCommand: ICommand);
    procedure Undo();
    procedure Redo();
    procedure Clear();
  end;

implementation

constructor TCommandManager.Create();
begin
  fCommands := TCommands.Create(True);
  fLastActionDescription := '';
end;

destructor TCommandManager.Destroy();
begin
  fCommands.Free;
  inherited;
end;

function TCommandManager.GetCanUndo: boolean;
begin
  GetCanUndo := fCommands.CanGoBack;
end;

function TCommandManager.GetCanRedo: boolean;
begin
  GetCanRedo := fCommands.CanGoForward;
end;

function TCommandManager.GetCurrent: ICommand;
begin
  GetCurrent := fCommands.Current;
end;

function TCommandManager.GetLastActionDescription: string;
begin
  GetLastActionDescription := fLastActionDescription;
end;

procedure TCommandManager.SetLastActionDescription(const aDescription: string);
begin
  fLastActionDescription := aDescription;
end;

procedure TCommandManager.Execute(aCommand: ICommand);
var
  success: boolean = False;
begin
  try
    success := aCommand.Execute();
  finally
    if success then
    begin
      LastActionDescription := aCommand.Description;
      fCommands.Add(aCommand);
    end
    else
      LastActionDescription := 'Error executing ' + aCommand.Description;
  end;
end;

procedure TCommandManager.Undo();
var
  command: ICommand;
  success: boolean = False;
begin
  if CanUndo then
  begin
    try
      command := fCommands.Current;
      success := fCommands.Current.Unexecute();
    finally
      fCommands.GoBack();
      if success then
        LastActionDescription := 'Undone: ' + command.Description
      else
      begin
        LastActionDescription := 'Error undoing: ' + command.Description;
        fCommands.Remove(command);
      end;
    end;
  end;
end;

procedure TCommandManager.Redo();
var
  command: ICommand;
  success: boolean = False;
begin
  if CanRedo then
  begin
    command := fCommands.GoForward;
    success := command.Execute();
    if success then
      LastActionDescription := 'Redone: ' + fCommands.Current.Description
    else
    begin
      LastActionDescription := 'Error redoing: ' + fCommands.Current.Description;
      fCommands.Remove(command);
    end;
  end;
end;

procedure TCommandManager.Clear();
begin
  fCommands.Clear;
end;

end.
