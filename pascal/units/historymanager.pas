unit HistoryManager;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Generics.Collections;

type

  generic THistoryManager<T> = class(TObject)
  type
    THistoryList = specialize TList<T>;

  private
    fItems: THistoryList;
    fCurrentIndex: integer;
    fMinIndex: integer;
    function GetIsEmpty: boolean;
    function GetCurrent: T;
    function GetCanGoBack: boolean;
    function GetCanGoForward: boolean;
  public
    property IsEmpty: boolean read GetIsEmpty;
    property Current: T read GetCurrent;
    property CanGoBack: boolean read GetCanGoBack;
    property CanGoForward: boolean read GetCanGoForward;
    constructor Create(const aAllowEmpty: boolean);
    destructor Destroy; override;
    procedure Add(const aItem: T);
    procedure Remove(const aItem: T);
    procedure Clear;
    function GoBack: T;
    function GoForward: T;
  end;

implementation

constructor THistoryManager.Create(const aAllowEmpty: boolean);
begin
  fItems := THistoryList.Create();
  fCurrentIndex := -1;
  if aAllowEmpty then
    fMinIndex := -1
  else
    fMinIndex := 0;
end;

destructor THistoryManager.Destroy;
begin
  fItems.Free();
  inherited;
end;

function THistoryManager.GetIsEmpty: boolean;
begin
  GetIsEmpty := fItems.Count = 0;
end;

function THistoryManager.GetCurrent: T;
begin
  if fCurrentIndex < 0 then
    GetCurrent := Default(T)
  else
    GetCurrent := fItems[fCurrentIndex];
end;

function THistoryManager.GetCanGoBack: boolean;
begin
  GetCanGoBack := fCurrentIndex > fMinIndex;
end;

function THistoryManager.GetCanGoForward: boolean;
begin
  GetCanGoForward := fCurrentIndex < fItems.Count - 1;
end;

procedure THistoryManager.Add(const aItem: T);
begin
  while (fItems.Count > 0) and (fItems.Count - 1 > fCurrentIndex) do
    fItems.Delete(fItems.Count - 1);
  fItems.Add(aItem);
  fCurrentIndex := fItems.Count - 1;
end;

procedure THistoryManager.Remove(const aItem: T);
var
  index: integer;
begin
  index := fItems.IndexOf(aItem);
  writeln('Index: ', index, '; >= 0: ', index >= 0);
  if index >= 0 then
  begin
    fItems.Remove(aItem);
    if index < fCurrentIndex then
      Dec(fCurrentIndex);
    if fCurrentIndex > (fItems.Count - 1) then
      fCurrentIndex := fItems.Count - 1;
  end;
end;

procedure THistoryManager.Clear;
begin
  fItems.Clear;
  fCurrentIndex := -1;
end;

function THistoryManager.GoBack: T;
begin
  if CanGoBack then
    Dec(fCurrentIndex);
  if fCurrentIndex < 0 then
    GoBack := Default(T)
  else
    GoBack := fItems[fCurrentIndex];
end;

function THistoryManager.GoForward: T;
begin
  if CanGoForward then
  begin
    Inc(fCurrentIndex);
    GoForward := fItems[fCurrentIndex];
  end
  else
    GoForward := Default(T);
end;

end.
