# mvpl - Move and Preserve Links

A kind of file manager which tries to keep symbolic links intact when moving or renaming files or directories which have links pointing at them. Currently implemented in [FreePascal](https://www.freepascal.org/)/[Lazarus](https://www.lazarus-ide.org/); other implementation might follow (just for the fun of it).

Currently Linux only due to Linux-specific calls for working with symlinks and trashing files, but could probably be extended to support other OSs as well -- as long as they support symbolic links and accessing the desktop environment's trashcan.

Furthermore, moving files/directories across file system boundaries is currently not supported. Shouldn't be hard to implement, though, since we already have recursive copy and delete functions!

## Features

* Typical file manager layout with a directory tree panel to the left and a directory contents list to the right
* Navigation:
    * Back/Forward
    * One level up
    * Open link target
* Undo/Redo functionality
* File operations:
    * Rename
    * Move to trash (requires `gio` to be installed and in `PATH`)
    * Delete permanently (not undoable -- as the name suggests ;)
    * Create new directory
* When dragging and dropping an item from the list onto a directory in either panel:
    * Move to target
    * (Recursively) Copy to target
    * Create relative symlink in target
    * Create absolute symlink in target

## Build

* Clone the repository
* Fire up Lazarus (3.0 or higher)
* Open the `mvpl.lpr` project file in the `pascal` subdirectory
* Chose your build mode (debug|release)
* When debugging, call the `./mktestroot` script from the project's base directory to re-create a test directory which will be pre-selected in debug mode
* Hit the run button and find the `mvpl` executable in the `pascal` subdirectory
